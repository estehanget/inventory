<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();

// Dashboard
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
// Store & Items Management
Route::resource('stores', 'StoreController');
Route::resource('items', 'ItemController');
Route::put('items/{id}/transfer', 'ItemController@transfer')->name('items.transfer');
Route::put('items/{id}/reduce', 'ItemController@reduce')->name('items.reduce');
// User Management
Route::resource('users', 'UserController');
// Unit Management
Route::resource('units', 'UnitController');
// Employee Management
Route::resource('employees', 'EmployeeController');
Route::put('employees/{id}/reset', 'EmployeeController@reset')->name('employees.reset');

Route::get('setting', 'SettingController@index')->name('setting');