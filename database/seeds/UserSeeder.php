<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name     = 'Admin';
        $user->email    = 'admin@mail.com';
        $user->role_id  = 1;
        $user->password = bcrypt('12345');
        $user->save();
    }
}
