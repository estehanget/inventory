<?php

use App\Store;
use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = new Store();
        $store->name = 'Toko A';
        $store->city = 'Yogyakarta';
        $store->district = 'Yogyakarta';
        $store->address = 'Jl. Yogyakarta';
        $store->save();
        
        $store = new Store();
        $store->name = 'Toko B';
        $store->city = 'Yogyakarta';
        $store->district = 'Yogyakarta';
        $store->address = 'Jl. Yogyakarta';
        $store->save();
        
        $store = new Store();
        $store->name = 'Toko C';
        $store->city = 'Yogyakarta';
        $store->district = 'Yogyakarta';
        $store->address = 'Jl. Yogyakarta';
        $store->save();
    }
}
