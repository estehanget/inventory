<?php

use App\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unit = new Unit();
        $unit->code_name = 'CPR';
        $unit->description = 'Cincin Perak';
        $unit->variety = 22;
        $unit->image = 'CPR.png';
        $unit->price = 100000;
        $unit->save();
        
        $unit = new Unit();
        $unit->code_name = 'CMS';
        $unit->description = 'Cincin Emas';
        $unit->variety = 23;
        $unit->image = 'CMS.png';
        $unit->price = 150000;
        $unit->save();
    }
}
