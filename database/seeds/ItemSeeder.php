<?php

use App\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item();
        $item->name = 'Cincin Emas';
        $item->store_id = 1;
        $item->unit_id = '2';
        $item->weight = 3;
        $item->stock = 10;
        $item->save();

        $item = new Item();
        $item->name = 'Cincin Perak';
        $item->store_id = 1;
        $item->unit_id = '1';
        $item->weight = 2;
        $item->stock = 13;
        $item->save();
    }
}
