<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    protected $path_file;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->path_file = storage_path('images');
    }

    public function index()
    {
        $units = Unit::all();
        return view('units.index', compact('units'));
    }
    
    public function create()
    {      
        return view('units.create');
    }
    
    public function store(Request $request)
    {
        $path = storage_path('app\public');
        $code_name = $request->input('code_name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');
        // image processing
        $format = $image->getClientOriginalExtension();
        $file_name = $code_name.'.'.$format;

        $request->validate([
            'code_name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'image'
        ],[
            'code_name.required' => 'jangan dikosongi!',
            'description.required' => 'jangan dikosongi!',
            'price.required' => 'jangan dikosongi!',
            'price.numeric' => 'format tidak sesuai!',
            'image.image' => 'format gambar tidak valid'
        ]);

        $unit = new Unit();
        $unit->code_name = $code_name;
        $unit->description = $description;
        $unit->image = $file_name;
        $unit->price = $price;
        $unit->save();

        $rename = $image->move($path, $file_name);

        return redirect('units');
    }

    public function edit($id)
    {
        $unit = Unit::find($id);

        return view('units.edit', compact('unit'));
    }

    public function update(Request $request, $id)
    {
        $path = storage_path('app\public');
        $code_name = $request->input('code_name');
        $description = $request->input('description');
        $price = $request->input('price');
        $image = $request->file('image');
        // image processing
        $format = $image->getClientOriginalExtension();
        $file_name = $code_name.'.'.$format;
        $rename = $image->move($path, $file_name);

        $request->validate([
            'code_name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'image' => 'image'
        ],[
            'code_name.required' => 'jangan dikosongi!',
            'description.required' => 'jangan dikosongi!',
            'price.required' => 'jangan dikosongi!',
            'price.numeric' => 'format tidak sesuai!'
        ]);

        $unit = Unit::find($id);
        $unit->code_name = $code_name;
        $unit->description = $description;
        $unit->image = $file_name;
        $unit->price = $price;
        $unit->save();

        return redirect('units');
    }
}
