<?php

namespace App\Http\Controllers;

use App\Item;
use App\Store;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $item = Item::find($id);
        $stores = Store::where('id', '!=', $item->store_id)->get();
        return view('items.edit', compact('item', 'stores'));
    }

    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $weight = $request->input('weight');

        $request->validate([
            'name' => 'required',
            'weight' => 'required|numeric|between:0,99.99'
        ]);

        $item = Item::find($id);
    }

    public function show($id)
    {
        $item = Item::find($id);
        return view('items.show', compact('item'));
    }

    public function reduce($id)
    {
        $item = Item::find($id);
    }
    
    public function transfer($id)
    {
        $item = Item::find($id);

    }

    public function destroy($id)
    {
        $item = Item::delete($id);
    }
}
