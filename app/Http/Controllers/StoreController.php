<?php

namespace App\Http\Controllers;

use App\Item;
use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $title = 'Daftar Toko';
        $href = '#';
        $icon = '';

        $stores = Store::all();

        return view('stores.index', compact('stores', 'title', 'href', 'icon'));
    }

    public function create()
    {
        $title = 'Tambah Data Toko';
        $href = route('stores.index');
        $icon = 'fa fa-arrow-left';

        return view('stores.create', compact('title', 'href', 'icon'));
    }

    public function store(Request $request)
    {
        $name = $request->input('store_name');
        $city = $request->input('city');
        $district = $request->input('district');
        $address = $request->input('address');

        $request->validate([
            'store_name' => 'required',
            'city' => 'required',
            'district' => 'required',
            'address' => 'required'
        ],[
            'store_name.required' => 'jangan dikosongi!',
            'city.required' => 'jangan dikosongi!',
            'district.required' => 'jangan dikosongi!',
            'address.required' => 'jangan dikosongi juga!'
        ]);

        $store = new Store();
        $store->name = $name;
        $store->city = $city;
        $store->district = $district;
        $store->address = $address;
        $store->save();

        return redirect('stores')->with('message', 'Data toko berhasil ditambahkan.');
    }

    public function edit($id)
    {
        $title = 'Edit Toko';
        $href = route('stores.index');
        $icon = 'fa fa-arrow-left';

        $store = Store::find($id);

        return view('stores.edit', compact('store', 'title', 'href', 'icon'));
    }

    public function update(Request $request, $id)
    {
        $name = $request->input('store_name');
        $city = $request->input('city');
        $district = $request->input('district');
        $address = $request->input('address');

        $request->validate([
            'store_name' => 'required',
            'city' => 'required',
            'district' => 'required',
            'address' => 'required'
        ],[
            'store_name.required' => 'jangan dikosongi!',
            'city.required' => 'jangan dikosongi!',
            'district.required' => 'jangan dikosongi!',
            'address.required' => 'jangan dikosongi juga!'
        ]);

        $store = Store::find($id);
        $store->name = $name;
        $store->city = $city;
        $store->district = $district;
        $store->address = $address;
        $store->save();

        $notify = true;

        return redirect('stores')->with(['message' => 'Data toko berhasil diupdate.']);

    }

    public function show($id)
    {
        $href = route('stores.index');
        $icon = 'fa fa-arrow-left';
        
        $store = Store::find($id);
        $items = Item::where('store_id', $id)->get();
        $title = $store->name;

        return view('stores.show', compact('store', 'items', 'title', 'href', 'icon'));
    }
}
