<?php

namespace App\Http\Controllers;

use App\User;
use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = 'Daftar Pegawai';
        $href = '#';
        $icon = '';

        $employees = User::where('role_id', 2)->get();

        return view('employees.index', compact('title', 'href', 'icon', 'employees'));
    }

    public function create()
    {
        $title = 'Tambah Pegawai';
        $href = route('employees.index');
        $icon = 'fa fa-arrow-left';

        return view('employees.create', compact('title', 'href', 'icon'));
    }

    public function store(Request $request)
    {
        $name = $request->input('employee_name');
        $email = $request->input('email');
        $password = $request->input('password');

        $request->validate([
            'employee_name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8'
        ],[
            'employee_name.required' => 'jangan dikosongi!',
            'email.required' => 'jangan dikosongi!',
            'email.email' => 'tidak sesuai format!',
            'password.required' => 'jangan dikosongi!',
            'password.min' => 'minimal 8 karakter!'
        ]);

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->role_id = 2;
        $user->password = bcrypt($password);
        $user->save();

        return redirect('employees')->with('message', 'Data pegawai berhasil ditambahkan');
    }

    public function edit($id)
    {
        $title = 'Edit Pegawai';
        $href = route('employees.index');
        $icon = 'fa fa-arrow-left';

        $employee = User::find($id);

        return view('employees.edit', compact('title', 'href', 'icon', 'employee'));
    }

    public function update(Request $request, $id)
    {
        $name = $request->input('employee_name');
        $email = $request->input('email');

        $employee = User::find($id);

        $request->validate([
            'employee_name' => 'required',
            'email' => 'unique:users,email,' . $employee->id,
        ],[
            'employee_name.required' => 'jangan dikosongi!',
            'email.required' => 'jangan dikosongi!',
            'email.email' => 'tidak sesuai format!',
            'email.unique' => 'sudah digunakan!',
            'password.required' => 'jangan dikosongi!',
            'password.min' => 'minimal 8 karakter!'
        ]);

        $user = User::find($id);
        $user->name = $name;
        $user->email = $email;
        $user->save();

        return redirect('employees')->with('message', 'Data pegawai berhasil diupdate!');
    }

    public function reset(Request $request, $id)
    {
        $password = $request->input('password');

        $request->validate([
            'password' => 'required|min:8'
        ],[
            'password.required' => 'jangan dikosongin!',
            'password.min' => 'minimal 8 karakter'
        ]);

        $user = User::find($id);
        $user->password = bcrypt($password);
        $user->save();

        return redirect('employees')->with('message', 'Password pegawai '.$user->name.' berhasil di update.');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('employees')->with('message', 'Data pegawai berhasil dihapus!');
    }
}
