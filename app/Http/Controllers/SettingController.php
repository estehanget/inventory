<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $title = 'Atur Produk';
        $href = '#';
        $icon = '';

        return view('setting.index', compact('title', 'href', 'icon'));
    }
}
