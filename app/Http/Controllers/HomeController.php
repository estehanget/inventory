<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = 'Dashboard';
        $href = '#';
        $icon = '';

        return view('dashboard.index', compact('title', 'href', 'icon'));
    }
}
