<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
