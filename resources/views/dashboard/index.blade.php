@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Panel</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3>Rp 999.999.999</h3>
                        <p>Omzet Hari Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-calculator"></i>
                    </div>
                    <a href="#" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3>999</h3>
                        <p>Penjualan Hari Ini</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <hr>
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="{{ route('stores.index') }}">
                    <div class="small-box bg-aqua">
                        <div class="inner" style="text-align:center">
                            <h3><i class="ion ion-bag"></i></h3>
                            <p>List Toko</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="{{ route('employees.index') }}">
                    <div class="small-box bg-aqua">
                        <div class="inner" style="text-align:center">
                            <h3><i class="ion ion-person"></i></h3>
                            <p>Manage Akun</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="{{ route('units.index') }}">
                    <div class="small-box bg-aqua">
                        <div class="inner" style="text-align:center">
                            <h3><i class="ion ion-cube"></i></h3>
                            <p>Manage Unit Item</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <a href="{{ route('units.index') }}">
                    <div class="small-box bg-aqua">
                        <div class="inner" style="text-align:center">
                            <h3><i class="fa fa-check-square"></i></h3>
                            <p>Lihat Absensi</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <a href="">
                    <div class="small-box bg-yellow">
                        <div class="inner" style="text-align:center">
                            <h3><i class="fa fa-sign-in"></i></h3>
                            <p>Laporan Pembelian</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <a href="">
                    <div class="small-box bg-yellow">
                        <div class="inner" style="text-align:center">
                            <h3><i class="fa fa-sign-out"></i></h3>
                            <p>Laporan Penjualan</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
