<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="control-sidebar">
                        <img src="{{ asset('assets/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset('assets/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->name }}
                                <small>{{ Auth::user()->role_id==1?'Admin':'' }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

@section('sidebar')
<aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
        <div class="tab-pane active">
            <ul class="control-sidebar-menu">
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-user bg-green"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Profile</h4>
                            <p>Halaman Edit Profile</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="{{ url('logout') }}">
                        <i class="menu-icon fa fa-sign-out bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Logout</h4>
                            <p>Keluar dari Dashboard</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>
<div class="control-sidebar-bg"></div>
@endsection
