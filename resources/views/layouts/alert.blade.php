@if(Session::has('success'))
<div class="alert alert-success alert-block" style="font-size:14px;text-align:center">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="fa fa-bell-alt"></i>{{Session::get('success')}}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger alert-block" style="font-size:14px;text-align:center">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="fa fa-bell-alt"></i>{{Session::get('error')}}
</div>
@endif
