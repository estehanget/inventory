<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Request::segment(1)=='dashboard' ? 'active' : '' }}">
                <a href="{{ url('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ Request::segment(1)=='stores' ? 'active' : '' }}">
                <a href="{{ url('stores') }}">
                    <i class="fa fa-home"></i> <span>Toko</span>
                </a>
            </li>
            <li class="{{ Request::segment(1)=='units' ? 'active' : '' }}">
                <a href="{{ url('units') }}">
                    <i class="fa fa-tags"></i> <span>Kategori Produk</span>
                </a>
            </li>
            <li class="{{ Request::segment(1)=='employees' ? 'active' : '' }}">
                <a href="{{ url('employees') }}">
                    <i class="fa fa-group"></i> <span>Man. User</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
