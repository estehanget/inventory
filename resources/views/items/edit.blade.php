@extends('layouts.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="{{ route('stores.show', $item->store_id) }}"></a>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('items.update', $item->id) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    <div class="form-group">
                        <label for="">
                            Nama Item
                            @if ($errors->has('name'))
                                <span class="text-warning" role="alert">{{ $errors->first('name') }}</span>
                            @endif
                        </label>
                        <input name="name" type="text" class="form-control" autocomplete="off" value="{{ empty(old('name')) ? $item->name : old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Berat (gram)
                            @if ($errors->has('weight'))
                                <span class="text-warning" role="alert">{{ $errors->first('weight') }}</span>
                            @endif
                        </label>
                        <input name="weight" type="text" class="form-control" autocomplete="off" value="{{ empty(old('weight')) ? $item->weight : old('weight') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-success btn-flat" type="submit"><i class="fa fa-check"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="progress progress-xxs">
            <div class="progress-bar progress-bar-striped progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#delete"><i class="fa fa-trash"></i> Hapus Item</button>
                <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#transfer"><i class="fa fa-exchange"></i> Transfer Item</button>
                <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#reduce"><i class="fa fa-minus-circle"></i> Kurangi Item</button>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{ route('items.destroy', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Hapus Item <strong>{{ $item->name }}</strong>?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-flat btn-danger">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="transfer" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{ route('items.transfer', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title">Transfer Item <strong>{{ $item->name }}</strong>?</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="">Toko Tujuan</label>
                                    <input type="text" class="form-control" value="{{ $item->store->name.' - '.substr($item->store->address, 0, 30) }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Toko Tujuan</label>
                                    <select class="form-control" id="store_id" name="store_id" style="width:100%">
                                        <option value="">Pilih Toko Tujuan</option>
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name.' - '.substr($store->address, 0, 30) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Jumlah Qty</label>
                                    <input type="number" name="quantity" class="form-control">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-flat btn-warning">Transfer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="reduce" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{ route('items.reduce', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Kurangi Item <strong>{{ $item->name }}</strong>?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-flat btn-danger">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

 @section('script')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#store_id').select2();
        });
    </script> 
 @endsection