@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Toko</li>
            <li class="active">Daftar Unit Item</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('units.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="">
                            Code Name
                            @if ($errors->has('code_name'))
                                <span class="text-warning" role="alert">{{ $errors->first('code_name') }}</span>
                            @endif
                        </label>
                        <input type="text" name="code_name" class="form-control" autocomplete="off" placeholder="Contoh: CPR">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Deskripsi
                            @if ($errors->has('description'))
                                <span class="text-warning" role="alert">{{ $errors->first('description') }}</span>
                            @endif
                        </label>
                        <input type="text" name="description" class="form-control" autocomplete="off" placeholder="Cincin Perak">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Harga per-gram
                            @if ($errors->has('price'))
                                <span class="text-warning" role="alert">{{ $errors->first('price') }}</span>
                            @endif
                        </label>
                        <input type="text" name="price" class="form-control" autocomplete="off">
                    </div>
                    <div class="form-group">
                    </div>
                    <ul class="products-list product-list-in-box">
                        <li class="item">
                            <div class="product-img">
                                <img src="{{ asset('assets/img/default-50x50.gif') }}" id="image" alt="Image">
                            </div>
                            <div class="product-info">
                                <input type="file" name="image" class="product-title form-control" id="image-input">
                                <p class="text-yellow">Notes: Rekomendasi ukuran gambar 100x100</p>
                                @if ($errors->has('image'))
                                    <span class="text-red" role="alert">{{ $errors->first('image') }}</span>
                                @endif
                            </div>
                        </li>
                    </ul>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-flat btn-block" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('style')
    <style>
        .products-list>.item {
            background: transparent;
        }
        .product-img {
            width: 150px;
        }
        .product-img #image {
            width: 100%;
            height: 100%;
        }
        .products-list .product-info {
            margin-left: 160px;
        }
    </style>
@endsection

@section('script')
    <script>
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#image-input').change(function() {
            readURL(this);
        });
    </script>
@endsection