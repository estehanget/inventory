@extends('layouts.template')

@section('style')
<style>
    .crop {
        width: 90px;
        height: 90px;
        overflow: hidden;
    }
    .crop img {
        width: 90px;
        height: 90px;
        margin: -7px 0 0 0;
    }        
</style>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <a href="{{ route('units.create') }}" class="btn btn-success btn-flat btn-block pull-right"><i class="fa fa-plus"></i> Tambah Kategori</a>                
            </div>
        </div>
    </section>
     <section class="content">
         <div class="row">
             @foreach ($units as $unit)
                <div class="col-lg-3">
                    <a href="{{ route('units.edit', $unit->id) }}" style="text-decoration:none;color:#000">
                        <div class="info-box">
                            <span class="info-box-icon">
                                <div class="crop">
                                    <img src="{{ asset('storage/'.$unit->image) }}" alt="">
                                </div>
                            </span>
                            <div class="info-box-content">
                                <span class="info-box-number">
                                    {{ $unit->code_name }}
                                </span>
                                <span class="info-box-description">
                                    {{ $unit->description }}<br>
                                    <span class="label label-info">Rp{{ number_format($unit->price) }}/gram</span>
                                    <span class="label label-warning">{{ $unit->variety }} Karat</span>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
             @endforeach
         </div>
     </section>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            f
        });
    </script>
@endsection