@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Toko</li>
            <li class="active">Tambah Baru</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('stores.store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="">
                            Nama Toko
                            @if ($errors->has('store_name'))
                                <span class="text-warning" role="alert">{{ $errors->first('store_name') }}</span>
                            @endif
                        </label>

                        <input type="text" name="store_name" class="form-control" value="{{ old('store_name') }}" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Kota/Kabupaten
                            @if ($errors->has('city'))
                                <span class="text-warning" role="alert">{{ $errors->first('city') }}</span>
                            @endif
                        </label>

                        <input type="text" name="city" class="form-control" value="{{ old('city') }}" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Kecamatan
                            @if ($errors->has('district'))
                                <span class="text-warning" role="alert">{{ $errors->first('district') }}</span>
                            @endif
                        </label>

                        <input type="text" name="district" class="form-control" value="{{ old('district') }}" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Alamat Lengkap
                            @if ($errors->has('address'))
                                <span class="text-warning" role="alert">{{ $errors->first('address') }}</span>
                            @endif
                        </label>

                        <textarea name="address" id="" cols="30" rows="3" class="form-control">{{ old('address') }}</textarea>
                    </div>

                    <button class="btn btn-success btn-flat btn-block" type="submit"><i class="fa fa-save"></i> Simpan</button>

                </form>
            </div>
        </div>
    </section>
@endsection
