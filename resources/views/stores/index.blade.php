@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-lg-2">
                <a href="{{ route('stores.create') }}" class="btn btn-success btn-flat btn-block"><i class="fa fa-plus"></i> Tambah Toko</a>
            </div>
        </div>
        {{-- <h1>
            Daftar Toko
            <div class="pull-right">
            </div>
        </h1> --}}
    </section>

    <section class="content">
        <div class="row">
            @foreach ($stores as $store)
                <a href="{{ route('stores.show', $store->id) }}">
                    <div class="col-lg-2 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua">
                                <i class="fa fa-home"></i>
                            </span>
                            <div class="info-box-content">
                                <span class="info-box-number">
                                    {{ $store->name }}
                                </span>
                                <span class="info-box-text">
                                    {{ $store->address }}
                                </span>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </section>
@endsection
