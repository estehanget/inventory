@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Toko</li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('stores.update', $store->id) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    <div class="form-group">
                        <label for="">
                            Nama Toko
                            @if ($errors->has('store_name'))
                                <span class="text-warning" role="alert">{{ $errors->first('store_name') }}</span>
                            @endif
                        </label>
                        <input type="text" name="store_name" class="form-control" autocomplete="off" value="{{ empty(old('store_name')) ? $store->name : old('store_name') }}">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Kota/Kabupaten
                            @if ($errors->has('city'))
                                <span class="text-warning" role="alert">{{ $errors->first('city') }}</span>
                            @endif
                        </label>

                        <input type="text" name="city" class="form-control" autocomplete="off" value="{{ empty(old('city')) ? $store->city : old('city') }}">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Kecamatan
                            @if ($errors->has('district'))
                                <span class="text-warning" role="alert">{{ $errors->first('district') }}</span>
                            @endif
                        </label>

                        <input type="text" name="district" class="form-control" autocomplete="off" value="{{ empty(old('district')) ? $store->district : old('district') }}">
                    </div>

                    <div class="form-group">
                        <label for="">
                            Alamat Lengkap
                            @if ($errors->has('address'))
                                <span class="text-warning" role="alert">{{ $errors->first('address') }}</span>
                            @endif
                        </label>

                        <textarea name="address" id="" cols="30" rows="3" class="form-control">{{ empty(old('address')) ? $store->address : old('address') }}</textarea>
                    </div>

                    <button class="btn btn-primary btn-fill btn-block" type="submit"><i class="fa fa-save"></i> Simpan</button>

                </form>
            </div>
        </div>
    </section>
@endsection
