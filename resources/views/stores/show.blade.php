@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href="{{ url('stores') }}"><i class="fa fa-arrow-left"></i></a>
            Item
            <small>{{ $store->name }}</small>

            <div class="pull-right">
                <a href="#" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Item</a>
            </div>
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @foreach ($items as $item)
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{ asset('storage/'.$item->unit->image) }}" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="{{ route('items.edit', $item->id) }}" class="product-title">{{ $item->name }}
                                            <span class="label label-warning pull-right">Rp{{ number_format($item->unit->price * $item->weight) }}</span>
                                            <span class="product-description">Stock: {{ $item->stock }}
                                                <span class="label label-info pull-right">{{ ($item->weight) }} gram</span>                                                
                                            </span>
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
