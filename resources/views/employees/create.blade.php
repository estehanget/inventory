@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Akun</li>
            <li>User</li>
            <li class="active">Tambah Baru</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('employees.store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="">
                            Nama Pengguna
                            @if ($errors->has('employee_name'))
                                <span class="text-warning" role="alert">{{ $errors->first('employee_name') }}</span>
                            @endif
                        </label>
                        <input name="employee_name" type="text" class="form-control" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Alamat Email
                            @if ($errors->has('email'))
                                <span class="text-warning" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </label>
                        <input name="email" type="text" class="form-control" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Password
                            @if ($errors->has('password'))
                                <span class="text-warning" role="alert">{{ $errors->first('password') }}</span>
                            @endif
                        </label>
                        <input name="password" type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-success btn-flat" type="submit"><i class="fa fa-check"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
