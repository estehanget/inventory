@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Akun</li>
            <li>User</li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('employees.update', $employee->id) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf

                    <div class="form-group">
                        <label for="">
                            Nama Pengguna
                            @if ($errors->has('employee_name'))
                                <span class="text-warning" role="alert">{{ $errors->first('employee_name') }}</span>
                            @endif
                        </label>
                        <input name="employee_name" type="text" class="form-control" autocomplete="off" value="{{ empty(old('employee_name')) ? $employee->name : old('employee_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="">
                            Alamat Email
                            @if ($errors->has('email'))
                                <span class="text-warning" role="alert">{{ $errors->first('email') }}</span>
                            @endif
                        </label>
                        <input name="email" type="text" class="form-control" autocomplete="off" value="{{ empty(old('email')) ? $employee->email : old('email') }}">
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <a href="{{ url('employees') }}" class="btn btn-default btn-flat btn-block"><i class="fa fa-arrow-left"></i> Kembali</a>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <button class="btn btn-block btn-primary btn-flat" type="submit"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#delete"><i class="fa fa-trash"></i> Hapus Data Pegawai</button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Hapus Pegawai <strong>{{ $employee->name }}</strong>?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-flat btn-danger">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <hr>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form action="{{ route('employees.reset', $employee->id) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">
                            Reset Password
                            @if ($errors->has('password'))
                                <span class="text-warning" role="alert">{{ $errors->first('password') }}</span>
                            @endif
                        </label>
                        <input type="password" class="form-control" name="password" placeholder="Masukkan password baru">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-fill btn-warning btn-block" type="submit"><i class="fa fa-check"></i> Reset Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
