@extends('layouts.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <a href="{{ route('employees.create') }}" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah User</a>

        <ol class="breadcrumb">
            <li><a href="{{ url('dashboard') }}"><i class="fa fa-user"></i> Home</a></li>
            <li>Akun</li>
            <li class="active">Pegawai</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            @foreach ($employees as $employee)
                <a href="{{ route('employees.edit', $employee->id) }}">
                    <div class="col-lg-3 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua">
                                <i class="fa fa-user"></i>
                            </span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    {{ $employee->name }}
                                </span>
                                <span class="info-box-number">
                                    {{ $employee->email }}
                                </span>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </section>
@endsection
